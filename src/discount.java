
public class discount {

	public double finalprice(int order) {
		
		
		double subtotal = 99 * order;	
		double discount = 0.0;
		
		if (order >= 10 && order <= 19)
		{  
			
			discount = subtotal * 0.20;
			
			}
		
		else if (order >= 20 && order <= 49)
		{  
			
			 discount = subtotal * 0.30;
			
			}
		
		else if (order >=50 && order <= 99)
		{
			discount = subtotal * 0.40;
		}
		
		else if ( order >= 100 )
		{
			discount = subtotal * 0.50;
		}
		
		else if (order < 0) {
			return -1;
		}
		
		
		double finaltotal = subtotal - discount;
		return finaltotal; 
		
		
	}

}
